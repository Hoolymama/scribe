# append SCRIBE_PROJECT_PATH bin paths to PATH
export PATH=$PATH:${SCRIBE_PROJECT_PATH}/bin



# append SCRIBE_PROJECT_PATH maya module path
if [[ -n $MAYA_MODULE_PATH ]] ;
   	then export MAYA_MODULE_PATH=${SCRIBE_PROJECT_PATH}/maya/modules:${MAYA_MODULE_PATH} ;
   	else export MAYA_MODULE_PATH=${SCRIBE_PROJECT_PATH}/maya/modules ;
fi
