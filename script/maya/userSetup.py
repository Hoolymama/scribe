import pymel.mayautils as pyu
import maya.mel as mm


def load_mod():
    tool = 'scribe'
    menu = 'Litter'
    mm.eval('jlib \"\"')
    mm.eval(tool + ' \"' + menu + '\"')

pyu.executeDeferred(load_mod)
